class Node:
    characters = []
    frequency = None
    left_child = None
    right_child = None

    def __init__(self, characters, frequency):
        self.characters = characters
        self.frequency = frequency

    def __str__(self):
        return "({}, {})".format("".join(self.characters), self.frequency)

    def get_encoding(self, current_encoding):
        if self.left_child:
            return self.left_child.get_encoding(current_encoding + "0")
        elif self.right_child:
            return self.right_child.get_encoding(current_encoding + "1")
        else:
            return (self.characters[0], current_encoding)


class HuffmanTree:
    root = None
    text = None
    encoded_characters = {}

    def __init__(self, text, build=True):
        self.text = text

        if build:
            self.build_tree()

    def _get_frequencies(self):
        frequencies = {}
        for character in self.text:
            if character not in frequencies:
                frequencies[character] = 1
            else:
                frequencies[character] += 1

        return frequencies

    def _get_initial_node_list(self):
        frequencies = self._get_frequencies()
        node_list = []

        for character, frequency in frequencies.items():
            node_list.append(Node([character], frequency))

        node_list.sort(key=lambda x: x.frequency, reverse=True)

        return node_list

    def build_tree(self):
        node_list = self._get_initial_node_list()

        while not len(node_list) == 1:
            right_child = node_list.pop()
            left_child = node_list.pop()
            new_parent = Node(characters=left_child.characters + right_child.characters,
                              frequency=left_child.frequency + right_child.frequency)
            new_parent.left_child = left_child
            new_parent.right_child = right_child

            node_list.append(new_parent)
            node_list.sort(key=lambda x: x.frequency, reverse=True)

        self.root = node_list.pop()

    def _get_encoding(self, current_node: Node, current_encoding=""):
        if not current_node.left_child and not current_node.right_child:
            self.encoded_characters[current_node.characters[0]
                                    ] = current_encoding
        else:
            if current_node.left_child:
                self._get_encoding(current_node.left_child,
                                   current_encoding + "0")

            if current_node.right_child:
                self._get_encoding(current_node.right_child,
                                   current_encoding + "1")

    def get_encoding(self):
        self.encoded_characters = {}
        self._get_encoding(self.root)
        return self.encoded_characters

    def encode(self):
        encoding = self.get_encoding()
        encoded_text = ""
        # Last encoded character from the text

        for character in self.text:
            encoded_text += encoding[character]

        # 11010011000001
        length_text = len(encoded_text)
        # 14

        tempt_text = encoded_text

        counter2 = 0
        counter = 0
        while length_text >= 8:
            length_text -= 8
            counter += 1

        _padding = 8 - (len(encoded_text) - 8*counter)

        while not len(tempt_text) % 8 == 0:
            tempt_text += "0"
            counter2 += 1

        encoding['padding'] = _padding

        return (encoded_text, encoding)


class HuffmanDecoder:
    encoded_text = None
    encoding = {}

    def __init__(self, encoded_text, encoded_characters) -> None:
        if "padding" in encoded_characters.keys():
            del encoded_characters["padding"]
        self.encoded_text = encoded_text
        self.encoding = {v: k for k, v in encoded_characters.items()}

    def decode(self):
        key = ""
        decoded_text = ""
        for bit in self.encoded_text:
            key += bit
            if key in self.encoding.keys():
                decoded_text += self.encoding[key]
                key = ""

        return decoded_text
