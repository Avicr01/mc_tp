# Prueba Momentaria
from tkinter import *
from tkinter import filedialog
import json
import sys
import os

from src.huffman import HuffmanTree, HuffmanDecoder

interface = Tk()


#------------Funcion que abre la ventana para Cargar un Archivo-------------



def openFileCompress():
    archivo = filedialog.askopenfilename(title="abrir",initialdir="C:/",filetypes=(("Archivos de Texto","*.txt"),("Todos los Archivos","*.*")))
    # Como string *probar*
    filenameC = archivo
    # Como lista
    # filenameC.set(archivo)



def openFileDecompress1():
    archivo = filedialog.askopenfilename(title="abrir",initialdir="C:/",filetypes=(("Archivos de Texto","*.txt"),("Todos los Archivos","*.*")))
    filenameD.set(archivo)

def openFileAlphabetize():
    archivo = filedialog.askopenfilename(title="abrir",initialdir="C:/",filetypes=(("Archivos de Texto","*.txt"),("Todos los Archivos","*.*")))
    filenameAlphabetize.set(archivo)
   

#-----------Funcion Comprimir------------------------------------------------

# TESTING
# Me imagino que una vez presiona el boton se invoca compress
def compress():
    if (filenameC != ""):
        # No realiza la accion de comprimir
        print("Ruta no ha sido puesta")
        sys.exit()
        
        input_directory, input_filename = os.path.split(filenameC)

        filename_no_extension, _ = os.path.splitext(filenameC) 
        
        with open(filenameC) as f:
            file_contents = f.read()
        text = file_contents
        ht = HuffmanTree(text)
        encoded_text, encoded_characters = ht.encode()

        # JSON Dictionary 
        
        out_encoding_path = os.path.join(
            input_directory, "{}.encoding.json".format(filename_no_extension))
        with open(out_encoding_path, 'w') as encoding_file:
            encoding_file.write(json.dumps(encoded_characters))

        # Binary file

        out_bin_path = os.path.join(
            input_directory, "cmp_{}".format(filename_no_extension))
        with open(out_bin_path, 'wb') as binary_file:
            binary_file.write(binary_string_to_bytearray(encoded_text))

        # Pop-Up Message




#---------------------------------------------------------------------------





imgBoton = PhotoImage(file="interface/btnDirectory.png")






#-------------Information of Interface-----------------



interface.resizable(0,0)
interface.title("Compresion y Descompresion de Archivos")
interface.iconbitmap("interface/boton_program.ico")



miFrame=Frame(interface,width=587,height=349)
miFrame.pack()




#-------------Welcome Message----------------------------------




LabelWelcome=Label(miFrame,text="¡Bienvenido al programa de compresión y descompresión de Huffman!")
LabelWelcome.place(x=10,y=10)

LabelOption = Label (miFrame,text="Elija opción desea realizar: ")
LabelOption.place(x=10,y=40)




#----------------Comprimir----------------------




buttonCompress = Button(interface,text="Comprimir")
buttonCompress.place(x=40,y=110)

LabelDirectory1 = Label (miFrame,text="Elija el archivo de texto que desea comprimir: ")
LabelDirectory1.place(x=200,y=80)

textboxCompress = Entry(miFrame,width=50,textvariable=filenameC)
textboxCompress.place(x=200,y=110)

buttonDirectory2 = Button(interface,image=imgBoton,command=openFileCompress,width=20,height=12).place(x=510,y=110)



#----------------------Descomprimir----------------------------------




buttonDecompress = Button(interface,text="Descomprimir")
buttonDecompress.place(x=30,y=250)

LabelDirectory2 = Label (miFrame,text="Elija el archivo de texto que desea comprimir: ")
LabelDirectory2.place(x=200,y=200)

textboxDecompress1 = Entry(miFrame,width=50,textvariable=filenameD)
textboxDecompress1.place(x=200,y=230)

buttonDirectory2 = Button(interface,image=imgBoton,command=openFileDecompress1,width=20,height=12).place(x=510,y=230)





LabelDirectory2_1 = Label (miFrame,text="Elija el archivo de diccionario: ")
LabelDirectory2_1.place(x=200,y=260)

textboxDecompress2 = Entry(miFrame,width=50,textvariable=filenameAlphabetize)
textboxDecompress2.place(x=200,y=290)

buttonDirectory3 = Button(interface,image=imgBoton,command=openFileAlphabetize,width=20,height=12).place(x=510,y=290)










interface.mainloop()
