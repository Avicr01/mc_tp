from tkinter import *
from tkinter import filedialog, messagebox
from huffman import HuffmanTree, HuffmanDecoder
import tkinter
import os
import json

def binary_string_to_bytearray(binary_string):
    """
    Transforms a binary string "01001100..." into a bytearray
    to be written into a binary file.
    """
    # Add 0 until the binary string has a length divisible by 8
    while not len(binary_string) % 8 == 0:
        binary_string += "0"

    # Divide the binary string into chunks of 8. For each chunk, cast it into
    # an integer and then convert the list of integers into a bytearray.
    return bytearray(int(binary_string[x:x+8], 2) for x in range(0, len(binary_string), 8))


def bytearray_to_binary_string(_bytearray, padding):
    """
    Transforms a bytearray into a binary string "01001100..." to be decoded
    by the HuffmanDecoder class.
    """
    # Convert the bytearray into a list of integers

    binary_string = ""
    for x in _bytearray:
        current_binary_string = format(x, 'b')
        # Complete with 0's to the left until the size is 8
        current_binary_string = '0' * \
            (8 - len(current_binary_string)) + current_binary_string
        binary_string += current_binary_string

    binary_string = binary_string[0:len(binary_string) - padding]

    return binary_string

interface = Tk()


#----Variables Strings Respectivos - NO EN USO POR EL MOMENTO - C PUEDE BORRAR ---------------

filenameC = StringVar()         #Directorio del archivo que se situa en el TextBox De Compress

filenameD = StringVar()         #Directorio del archivo que se situa en el TextBox De Decompress

filenameAlphabetize = StringVar()        #Directorio del archivo que se situa en el TextBox De Alphabetize

fileContent =StringVar()                 #String que almacena todo el texto del archivo


#------------Funcion que abre la ventana para Cargar un Archivo-------------



def openFileCompress():
    archivo = filedialog.askopenfilename(title="abrir",initialdir="C:/",filetypes=(("Archivos de Texto","*.txt"),("Todos los Archivos","*.*")))
    filenameC.set(archivo)


def openFileDecompress1():
    archivo = filedialog.askopenfilename(title="abrir",initialdir="C:/")
    filenameD.set(archivo)

def openFileAlphabetize():
    archivo = filedialog.askopenfilename(title="abrir",initialdir="C:/",filetypes=(("Archivos JSON","*.json"),("Todos los Archivos","*.*")))
    filenameAlphabetize.set(archivo)
   

#-----------Funcion Comprimir------------------------------------------------

def compress(filename):
    if (filename == ''):
        tkinter.messagebox.showinfo("Error", "No ha seleccionado un documento")
    else:
        input_directory, input_filename = os.path.split(filename)

        filename_no_extension, _ = os.path.splitext(input_filename)

        with open(filename) as f:
            file_contents = f.read()
        text = file_contents
        ht = HuffmanTree(text)
        encoded_text, encoded_characters = ht.encode()

        out_bin_path = os.path.join(
            input_directory, "cmp_{}".format(filename_no_extension))
        out_encoding_path = os.path.join(
            input_directory, "{}.encoding.json".format(filename_no_extension))

        with open(out_bin_path, 'wb') as binary_file:
            binary_file.write(binary_string_to_bytearray(encoded_text))
        with open(out_encoding_path, 'w') as encoding_file:
            encoding_file.write(json.dumps(encoded_characters))

        tkinter.messagebox.showinfo("Archivo comprimido", "Archivo comprimido en {}".format(out_bin_path) + \
            "\nCodificación guardada en {}".format(out_encoding_path))



#-----------Funcion Comprimir------------------------------------------------

def decompress(binary, alphabetize):
    if (binary == '' or alphabetize == ''):
        tkinter.messagebox.showinfo("Error", "No ha seleccionado un documento")
    else:
        decoded_path, _ = os.path.split(binary)

        filename_no_extension, _ = os.path.splitext(binary)

        _bytearray = bytearray()

        with open(binary, 'rb') as f_et:
            byte = f_et.read(1)
            while byte and byte != b"":
                _bytearray.append(int.from_bytes(byte, "big"))
                byte = f_et.read(1)
        with open(alphabetize, 'r') as encoding_file:
            dic_content = encoding_file.read()

        # Decoding del JSON
        dic_content = json.loads(dic_content)

        padding = dic_content['padding']

        # Decoding binary - In progress
        _bytearray = bytearray_to_binary_string(_bytearray, padding)

        hd = HuffmanDecoder(_bytearray, dic_content)
        decoded_text = hd.decode()

        # New path name
        out_decoded_path = os.path.join(
            decoded_path, "decoded.txt".format(filename_no_extension))

        with open(out_decoded_path, 'w') as text_file:
            text_file.write((decoded_text))

        tkinter.messagebox.showinfo("Archivo descomprimido", "Archivo descomprimido en {}".format(out_decoded_path))

#---------------------------------------------------------------------------





imgBoton = PhotoImage(file="interface/btnDirectory.png")






#-------------Information of Interface-----------------



interface.resizable(0,0)
interface.title("Compresion y Descompresion de Archivos")
interface.iconbitmap("interface/boton_program.ico")



miFrame=Frame(interface,width=587,height=349)
miFrame.pack()




#-------------Welcome Message----------------------------------




LabelWelcome=Label(miFrame,text="¡Bienvenido al programa de compresión y descompresión de Huffman!")
LabelWelcome.place(x=10,y=10)

LabelOption = Label (miFrame,text="Elija opción desea realizar: ")
LabelOption.place(x=10,y=40)




#----------------Comprimir----------------------




buttonCompress = Button(interface,text="Comprimir", command=lambda:
                        compress(filenameC.get()))
buttonCompress.place(x=40,y=110)

LabelDirectory1 = Label (miFrame,text="Elija el archivo de texto que desea comprimir: ")
LabelDirectory1.place(x=200,y=80)

textboxCompress = Entry(miFrame,width=50,textvariable=filenameC)
textboxCompress.place(x=200,y=110)

buttonDirectory2 = Button(interface,image=imgBoton,command=openFileCompress,width=20,height=12).place(x=510,y=110)



#----------------------Descomprimir----------------------------------




buttonDecompress = Button(interface,text="Descomprimir", command=lambda:
                          decompress(filenameD.get(), filenameAlphabetize.get()))
buttonDecompress.place(x=30,y=250)

LabelDirectory2 = Label(miFrame,text="Elija el archivo de texto que desea descomprimir: ")
LabelDirectory2.place(x=200,y=200)

textboxDecompress1 = Entry(miFrame,width=50,textvariable=filenameD)
textboxDecompress1.place(x=200,y=230)

buttonDirectory2 = Button(interface,image=imgBoton,command=openFileDecompress1,width=20,height=12).place(x=510,y=230)





LabelDirectory2_1 = Label (miFrame,text="Elija el archivo de diccionario: ")
LabelDirectory2_1.place(x=200,y=260)

textboxDecompress2 = Entry(miFrame,width=50,textvariable=filenameAlphabetize)
textboxDecompress2.place(x=200,y=290)

buttonDirectory3 = Button(interface,image=imgBoton,command=openFileAlphabetize,width=20,height=12).place(x=510,y=290)










interface.mainloop()
