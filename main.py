import json
import os
import sys

from src.huffman import HuffmanTree, HuffmanDecoder


def clear():
    # "nt" means Windows
    return
    # return os.system("cls" if os.name == "nt" else "clear")


def binary_string_to_bytearray(binary_string):
    """
    Transforms a binary string "01001100..." into a bytearray
    to be written into a binary file.
    """
    # Add 0 until the binary string has a length divisible by 8
    while not len(binary_string) % 8 == 0:
        binary_string += "0"

    # Divide the binary string into chunks of 8. For each chunk, cast it into
    # an integer and then convert the list of integers into a bytearray.
    return bytearray(int(binary_string[x:x+8], 2) for x in range(0, len(binary_string), 8))


def bytearray_to_binary_string(_bytearray, padding):
    """
    Transforms a bytearray into a binary string "01001100..." to be decoded
    by the HuffmanDecoder class.
    """
    # Convert the bytearray into a list of integers

    binary_string = ""
    for x in _bytearray:
        current_binary_string = format(x, 'b')
        # Complete with 0's to the left until the size is 8
        current_binary_string = '0' * \
            (8 - len(current_binary_string)) + current_binary_string
        binary_string += current_binary_string

    binary_string = binary_string[0:len(binary_string) - padding]

    return binary_string


if __name__ == "__main__":
    from colorama import Fore

    while True:
        print(Fore.LIGHTBLUE_EX + "\nMENU DE OPCIONES")
        print(Fore.RESET + "Seleccione la opcion que quiere realizar: ")
        print("1. Codificar un archivo")
        print("2. Decodificar un archivo")
        print("3. Salir")

        option = int(input('Elija su opcion: '))
        clear()

        if option == 1:
            # Compress a file
            # Input the route of the file
            input_path = input('Ingrese la ruta de su archivo: ')

            if not os.path.exists(input_path):
                print("Error, la ruta de archivo {} no existe".format(input_path))
                sys.exit()

            input_directory, input_filename = os.path.split(input_path)

            filename_no_extension, _ = os.path.splitext(input_filename)

            with open(input_path) as f:
                file_contents = f.read()
            text = file_contents
            ht = HuffmanTree(text)
            encoded_text, encoded_characters = ht.encode()

            out_bin_path = os.path.join(
                input_directory, "cmp_{}".format(filename_no_extension))
            out_encoding_path = os.path.join(
                input_directory, "{}.encoding.json".format(filename_no_extension))

            with open(out_bin_path, 'wb') as binary_file:
                binary_file.write(binary_string_to_bytearray(encoded_text))
            with open(out_encoding_path, 'w') as encoding_file:
                encoding_file.write(json.dumps(encoded_characters))

            print(Fore.YELLOW + "\nArchivo comprimido en {}".format(out_bin_path))
            print(Fore.YELLOW +
                  "\nCodificación guardada en {}".format(out_encoding_path))

        elif option == 2:
            # Uncompress a file
            # Input the route of the file
            binary_file_route = input(
                'Ingrese la ruta del archivo binario a descomprimir: ')
            encoding_json_route = input(
                'Ingrese la ruta del archivo de codificación de caracteres (.json): ')

            for route in [binary_file_route, encoding_json_route]:
                if not os.path.exists(route):
                    print("Error: El archivo {} no existe.".format(route))
                    sys.exit()

            # Decoding in progress

            decoded_path, _ = os.path.split(binary_file_route)

            filename_no_extension, _ = os.path.splitext(binary_file_route)

            _bytearray = bytearray()

            with open(binary_file_route, 'rb') as f_et:
                byte = f_et.read(1)
                while byte and byte != b"":
                    _bytearray.append(int.from_bytes(byte, "big"))
                    byte = f_et.read(1)
            with open(encoding_json_route, 'r') as encoding_file:
                dic_content = encoding_file.read()

            # Decoding del JSON
            dic_content = json.loads(dic_content)

            padding = dic_content['padding']

            # Decoding binary - In progress
            _bytearray = bytearray_to_binary_string(_bytearray, padding)

            hd = HuffmanDecoder(_bytearray, dic_content)
            decoded_text = hd.decode()

            # New path name
            out_decoded_path = os.path.join(
                decoded_path, "decoded.txt".format(filename_no_extension))

            with open(out_decoded_path, 'w') as text_file:
                text_file.write((decoded_text))

            print(Fore.YELLOW + "\nArchivo descomprimido")

        elif option == 3:
            # Salir
            sys.exit()
