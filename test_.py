from src.huffman import HuffmanTree, HuffmanDecoder
import main


def test_get_frequencies():
    text = "mississippi river"
    ht = HuffmanTree(text)

    frequencies = ht._get_frequencies()

    assert sum(frequencies.values()) == len(text)

    assert frequencies == {
        'm': 1,
        'i': 5,
        's': 4,
        'p': 2,
        ' ': 1,
        'r': 2,
        'e': 1,
        'v': 1
    }


def test_get_initial_node_list():
    text = "mississippi river"
    ht = HuffmanTree(text)

    node_list = ht._get_initial_node_list()

    sorted_frequencies = [node.frequency for node in node_list]

    assert sorted_frequencies == [5, 4, 2, 2, 1, 1, 1, 1]


def test_build_tree():
    text = "mississippi river"
    ht = HuffmanTree(text, build=False)

    ht.build_tree()

    assert ht.root.frequency == len(text)


def test_build_tree_detail():
    text = "mississippi river"
    ht = HuffmanTree(text)

    # Top level
    assert set(ht.root.characters) == set(
        ['i', 's', 'p', 'r', 'm', 'v', 'e', ' '])
    assert ht.root.frequency == len(text)

    # Level 2
    left_child = ht.root.left_child
    right_child = ht.root.right_child

    assert set(left_child.characters) == set(['i', 's'])
    assert left_child.frequency == 9

    assert set(right_child.characters) == set(['p', 'r', 'm', 'v', 'e', ' '])
    assert right_child.frequency == 8


def test_encode_decode():
    text = "mississippi river"
    ht = HuffmanTree(text)

    encoded_text, encoded_characters = ht.encode()
    decoder = HuffmanDecoder(encoded_text, encoded_characters)

    assert decoder.decode() == text


def test_encode_decode_lower_upper():
    text = "miSsisSipPI rIVeR"
    ht = HuffmanTree(text)

    encoded_text, encoded_characters = ht.encode()
    decoder = HuffmanDecoder(encoded_text, encoded_characters)

    assert decoder.decode() == text


def test_check_unique_encoding():
    text = "miSsisSipPI rIVeR"
    ht = HuffmanTree(text)

    _, encoded_characters = ht.encode()

    for i, current_encoding in enumerate(encoded_characters.values()):
        for j, encoding in enumerate(encoded_characters.values()):
            if i != j and type(current_encoding) != int and type(encoding) != int:
                assert not encoding.startswith(current_encoding)


def test_binary_string_to_bytearray_and_back():
    bin_string = "100100"
    bytearr = main.binary_string_to_bytearray(bin_string)

    assert main.bytearray_to_binary_string(bytearr, 2) == bin_string


def test_binary_string_to_bytearray_and_back_with_long_text():
    book = ""
    with open("./text_folder/input.txt") as book_file:
        for line in book_file:
            book += line

    ht = HuffmanTree(book)
    bin_string, _ = ht.encode()

    bytearr = main.binary_string_to_bytearray(bin_string)
    assert main.bytearray_to_binary_string(bytearr, 4) == bin_string


def test_encode_decode_long_text():
    book = ""
    with open("./text_folder/input.txt") as book_file:
        for line in book_file:
            book += line

    ht = HuffmanTree(book)

    encoded_text, encoded_characters = ht.encode()

    decoder = HuffmanDecoder(encoded_text, encoded_characters)

    decoded_text = decoder.decode()

    assert len(book) == len(decoded_text)
    assert book[:100] == decoded_text[:100]
    assert book[len(book) - 101:] == decoded_text[len(book) - 101:]
    assert book == decoded_text
