def count_letters(word, char):
    count = 0
    for c in word:
        if char == c:
            count += 1
    return count

def count_spaces(word):
    count = 0
    for i in range(0, len(word)):
        if word[i] == " ":
            count += 1
    return count

traba = "Tres tristes tigres"
freq = 0

print("\n\n")
print("\t\t\tCONTADOR DE LETRAS\n")
print("\t\t   El texto ingresado es:    \n\t\t  ", traba)
print("\t\t________________________________")
for i in range(65, 91):
    get = count_letters(traba, chr(i))
    if get != 0:
        print("\t\t|\t", chr(i), "\t|\t", get, "\t|")
    freq = freq + get

for i in range(97, 123):
    get = count_letters(traba, chr(i))
    if get != 0:
        print("\t\t|\t", chr(i), "\t|\t", get, "\t|")
    freq = freq + get

get = count_spaces(traba)
print("\t\t|\t", "\" \"", "\t|\t", get, "\t|")
freq = freq + get
print("\t\t________________________________")

print("\t\tNumero de caracteres: ", freq)
print("\t\tCantidad de bits de los caracteres: ", freq*8)
print("\n\n")

